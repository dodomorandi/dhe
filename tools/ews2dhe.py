#!/usr/bin/env python3
"""Converts the file format used in EWS to the DHE json format"""

import re
import os
import warnings
from dataclasses import dataclass, fields
import numpy

from dhe_o3.model import DHEConfiguration, T_brine_calc_method, \
    SoilLayerProperties, GCone, GFunc, PCalculationMode, DHE


def warning_on_one_line(message, category, *_args, **_kwargs):
    return '{}: {}\n'.format(category.__name__, message)


warnings.formatwarning = warning_on_one_line


def str2bool(s):
    return {b"FALSE": 0.0, b"TRUE": 1.0}[s]


header_info = ((b"Step", "t [s]", float),
               (b"M [kg/s]", "M [kg/s]", float),
               (b"TSink", "T_sink [°C]", float),
               (b"Q [kW]", "P [W]", float),
               (b"TSource", "T_source [°C]", float),
               (b"TMonitor", "T_monitor [°C]", float),
               (b"p [Pa]", "p [Pa]", float),
               (b"laminar?", "laminar", str2bool)
               )
header_mapping = {src: dest for src, dest, _ in header_info}
converter_mapping = {src: cvt for src, _,
                     cvt in header_info if cvt != float}


@dataclass
class Variable:
    name: str
    value: object
    unit: str
    description: str


def convert(input_file, config, out, profile_file_name):
    with open(input_file, "rb") as stream:
        variables_raw = parse_config(stream)
        if config is not None:
            variables_raw = tuple(variables_raw)
            D = read_config(variables_raw, profile_file_name)
            DHEConfiguration.save(D, config)
        variables = {v.name: v.value for v in variables_raw}
        t = variables["Simulationsdauer"] * 3600  # h -> sec
        header, data = read_profile(
            stream, n_rows=variables["numrows"], t=t)
    for output_file, prepare_data in out:
        out_header, out = prepare_data(header, data)
        delimiter = ";"
        numpy.savetxt(output_file, out, delimiter=delimiter,
                      fmt="%g", header=delimiter.join(out_header))


def read_profile(stream, n_rows, t):
    line = stream.readline()
    header = line.rstrip(b"\r\n").split(b"\t")
    converters = {i: converter_mapping.get(col)
                  for i, col in enumerate(header)
                  if col in converter_mapping}
    data = numpy.loadtxt(stream, delimiter="\t",
                         converters=converters)

    n = data.shape[0]
    assert n == n_rows
    P_idx = header.index(b"Q [kW]")
    data[:, 0] -= 1
    data[:, 0] *= (t/n)
    data[:, P_idx] *= 1000.  # kW -> W
    return tuple(header_mapping[h] for h in header), data


def prepare_data_P(header, data):
    P_idx = header.index("P [W]")
    out = data[:, (0, P_idx)]
    out_header = (header[0], header[P_idx])
    return out_header, out


def parse_config(stream):
    for _ in range(5):
        stream.readline()
    pos = stream.tell()
    for line in stream:
        if line.startswith(b"Step"):
            stream.seek(pos)
            break
        line = line.strip(b" \r\n")
        if not line:
            continue
        cols = line.split(b"\t")
        parser = parsers_by_n_cols.get(len(cols))
        if parser is not None:
            yield from parser(*cols)
        else:
            warnings.warn("Unparsed line: " + line.decode("ISO-8859-1"))
        pos = stream.tell()


def var_setter(dest_obj, dest_attr):
    def _set(src):
        setattr(dest_obj, dest_attr, src.value)
    return _set


def var_setter_scaled(dest_obj, dest_attr, scale=1.0):
    def _set(src):
        setattr(dest_obj, dest_attr, src.value*scale)
    return _set


def var_adder(dest_obj, dest_attr):
    def _set(src):
        setattr(dest_obj, dest_attr, getattr(dest_obj, dest_attr) + src.value)
    return _set


def arr_size_setter(dest_obj, dest_attr, fill_object):
    def _set(src):
        arr = getattr(dest_obj, dest_attr)
        if src.value > len(arr):
            arr += type(arr)((fill_object,))*(src.value - len(arr))
        elif src.value < len(arr):
            del arr[src.value:]

    return _set


def enum_setter(dest_obj, dest_attr, mapping, default):
    def _set(src):
        try:
            val = mapping[src.value]
        except KeyError:
            warnings.warn("Unexpected value for {}.{}: '{}'".format(
                dest_obj, dest_attr, src.value))
            val = default
        setattr(dest_obj, dest_attr, val)
    return _set


def arr_item_setter(dest_obj, dest_attr):
    def _set(src, i):
        if i <= len(dest_obj):
            setattr(dest_obj[i-1], dest_attr, src.value)
    return _set


def arr_item_setter_static(dest_obj, i):
    def _set(src):
        dest_obj[i] = src.value
    return _set


default_g_method = next(f for f in fields(
    DHEConfiguration) if f.name == "g_method").default


def read_config(variables, profile_file_name):
    env = DHEConfiguration()
    env.soil_parameters.T_soil_mean = 0.
    dhe = DHE(D_borehole=None, D=None, L=None)
    calc_method_P = PCalculationMode(load_file=profile_file_name)
    g_func = GFunc()
    g_func.g_coefs = list(g_func.g_coefs)
    var_mapping = {
        "Sondendurchmesser": var_setter(dhe, "D"),
        "Dicke_Sondenrohr": var_setter(dhe, "thickness"),
        "Sondenlaenge": var_setter(dhe, "L"),
        "Bohrdurchmesser": var_setter(dhe, "D_borehole"),
        "Jahresmitteltemp": var_adder(env.soil_parameters, "T_soil_mean"),
        "Bodenerwaermung": var_adder(env.soil_parameters, "T_soil_mean"),
        "TGrad": var_setter(env.soil_parameters, "T_grad"),
        "Massenstrom": var_setter(dhe, "Phi_m"),
        "Sicherheit2": var_setter(env, "opt_n_steps_multiplier"),
        "Rechenradius": var_setter(env, "R"),
        "Gitterfaktor": var_setter(env, "Gamma"),
        "adiabat": var_setter(env, "adiabat"),
        "Genauigkeit": var_setter(calc_method_P.arguments, "precision"),
        "Sicherheit1": var_setter(env, "n_steps_0"),
        "Zeitschritt": var_setter_scaled(env, "dt", scale=60.),
        "Ra": var_setter(dhe, "Ra"),
        "Rb": var_setter(dhe, "Rb"),
        "R1": var_setter(dhe, "R1"),
        "DimRad": var_setter(env, "dim_rad"),
        "lambdaSole": var_setter(dhe.brine_properties, "lambda_"),
        "rhoSole": var_setter(dhe.brine_properties, "rho"),
        "cpSole": var_setter(dhe.brine_properties, "c"),
        "nueSole": var_setter(dhe.brine_properties, "nu"),
        "lambdaFill": var_setter(dhe.fill_properties, "lambda_"),
        "rhoFill": var_setter(dhe.fill_properties, "rho"),
        "cpFill": var_setter(dhe.fill_properties, "c"),
        "g1": arr_item_setter_static(g_func.g_coefs, 0),
        "g2": arr_item_setter_static(g_func.g_coefs, 1),
        "g3": arr_item_setter_static(g_func.g_coefs, 2),
        "g4": arr_item_setter_static(g_func.g_coefs, 3),
        "g5": arr_item_setter_static(g_func.g_coefs, 4),
        "Sondenabstand": var_setter(g_func, "d_DHE"),
        "g_Sondenabstand": var_setter(g_func, "d_DHE_ref"),
        "gfunction": enum_setter(dhe, "g_method", {False: GCone(),
                                                   True: g_func},
                                 default=default_g_method),
        "stationaer": enum_setter(env, "T_brine_method", {
            False: T_brine_calc_method.dynamic,
            True: T_brine_calc_method.stationary},
            default=T_brine_calc_method.dynamic),
        "Leistung": enum_setter(env, "calculation_mode",
                                {True: calc_method_P},
                                default=calc_method_P),
        "DimAxi": arr_size_setter(
            env, "soil_layers",
            fill_object=SoilLayerProperties(d=None, rho=None,
                                            c=None, lambda_=None))
    }
    arr_mapping = {
        "rhoErdeSchicht": arr_item_setter(env.soil_layers, "rho"),
        "cpErdeSchicht": arr_item_setter(env.soil_layers, "c"),
        "lambdaErdeSchicht": arr_item_setter(env.soil_layers, "lambda_"),
        "Schicht": arr_item_setter(env.soil_layers, "d")
    }

    for v in variables:
        if v.name.endswith("]"):
            arr_name, i = v.name.rstrip("]").split("[")
            setter = arr_mapping.get(arr_name)
            if setter:
                setter(v, int(i))
        else:
            setter = var_mapping.get(v.name)
            if setter:
                setter(v)
    env.dim_ax = len(env.soil_layers)
    env.dhe = [dhe]

    g_func.L = dhe.L

    dhe.T_soil_0_parameters.d_DHE = g_func.d_DHE
    dhe.T_soil_0_parameters.g_coefs = g_func.g_coefs

    return env


def line_parse_5(val, unit, description, _, var_name):
    yield Variable(var_name.decode(), parse_value(val),
                   unit.decode("ISO-8859-1"),
                   description.decode("ISO-8859-1"))


def parse_value(s):
    bool_map = {b"FALSE": False, b"TRUE": True}
    if s in bool_map:
        return bool_map[s]
    if b"." in s:
        return float(s)
    return int(s)


def line_parse_n(*args):
    desc_re = re.compile(r"\[([^]]*)\], ")
    * val, description, var_name = args
    description = description.decode("ISO-8859-1")
    unit_match = desc_re.match(description)
    unit = unit_match and unit_match.group(1)
    if unit:
        description = description[unit_match.span()[-1]:]
    descriptions = tuple(map(lambda s: s.strip(), description.split("/")))
    var_names = tuple(map(lambda s: s.strip(), var_name.decode().split("/")))
    if len(descriptions) < len(var_names):
        descriptions += (None,)*(len(var_names) - len(descriptions))
    return (Variable(v, val, unit, desc)
            for v, val, desc in zip(var_names, val, descriptions))


def line_parse_7(*args):
    return line_parse_n(*(args[:-2] + args[-1:]))


parsers_by_n_cols = {5: line_parse_5,
                     6: line_parse_n,
                     7: line_parse_7,
                     10: line_parse_n}


def relative_path(path, start):
    if start is None:
        return path
    if path is None:
        return None
    start = os.path.realpath(start)
    path = os.path.realpath(path)
    return os.path.relpath(path, start=os.path.dirname(start))


if __name__ == "__main__":
    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument("input_file",
                           help="Input file")
    argparser.add_argument("--profile", default=None,
                           help="Profile output file (default: no output)")
    argparser.add_argument("--config", default=None,
                           help="Config output file (default: no output)")
    argparser.add_argument("--reference", default=None,
                           help="Reference profile output file "
                           "(default: no output)")
    argparser.add_argument("-O", default=None,
                           help="Output directory")
    cmd_args = argparser.parse_args()
    if (cmd_args.O and cmd_args.config
        and os.path.realpath(cmd_args.O) ==
            os.path.realpath(os.path.dirname(cmd_args.config))):
        raise FileExistsError(
            "input_file and config output file are the same!")
    _out = []
    _base = cmd_args.O and os.path.join(
        cmd_args.O, os.path.splitext(os.path.basename(cmd_args.input_file))[0])
    _reference = cmd_args.reference or (_base and _base + "_ref.csv")
    if _reference is not None:
        _out.append((_reference, lambda *args: args))
    _profile = cmd_args.profile or (_base and _base + ".csv")
    if _profile is not None:
        _out.append((_profile, prepare_data_P))
    _config = cmd_args.config or (_base and _base + ".json")
    convert(cmd_args.input_file,
            config=_config,
            out=_out,
            profile_file_name=relative_path(_profile, _config))
